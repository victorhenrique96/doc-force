@extends('site.layout.main')

@section('title', 'Busca de Documentos')
@section('meta-desc', 'Busca de documentos')

@section('breadcrumb')
<li class="breadcrumb-item active" aria-current="page">
	<a href="{{ url('/busca-de-documentos') }}">@yield('title')</a>
</li>
@endsection

@section('content')

<div class="row produtos page bg-about" id="Certidoes">
	<div class="container">
		<div class="row page-title">
			<div class="container">
				@include('site.includes.breadcrumb')
			</div>
		</div>

		<div class="row-fluid certidoes">
			<h3 class="title">
				<strong>Certidão por Países</strong>
			</h3>
		</div>

		<div class="row certidoes-list">

			<div class="col-sm-12 col-md-6 no-pad">
				<a href="{{url('/busca-de-documentos/brasil')}}">
					<div class="card box">
						<div class="content">
							<h3 class="title brazil">Busca de Certidões<br>no Brasil</h3>
							<p class="description">
								As certidões e registros históricos no Brasil dos seus antepassados, que não estão no acervo da família, podem ser recuperadas com nossa equipe.
								{{-- A docforce realiza a busca por documentos<br>
								históricos em outros países e os entrega na sua<br>casa. --}}
							</p>
							<div class="btn-area">
								<span class="btn btn-certidao">Saiba mais</span>
							</div>
						</div>
					</div>
				</a>
			</div>

			<div class="col-sm-12 col-md-6 no-pad">
				<a href="{{url('/busca-de-documentos/espanha')}}">
					<div class="card box">
						<div class="content">
							<h3 class="title spain">Busca de Certidões<br>na Espanha</h3>
							<p class="description">
								Devido a imigração europeia que ocorreu entre os séculos XIX e XX, estima-se que aproximadamente 15 milhões de brasileiros tenham descendência espanhola.
								{{-- A docforce realiza a busca por documentos<br>
								históricos em outros países e os entrega na sua<br>casa. --}}
							</p>
							<div class="btn-area">
								<span class="btn btn-certidao">Saiba mais</span>
							</div>
						</div>
					</div>
				</a>
			</div>

			<div class="col-sm-12 col-md-6 no-pad">
				<a href="{{url('/busca-de-documentos/italia')}}">
					<div class="card box">
						<div class="content">
							<h3 class="title italy">Busca de Certidões<br>na Itália</h3>
							<p class="description">
								A Itália é um dos países mais procurados pelos brasileiros para o reconhecimento da dupla cidadania.
								{{-- A docforce realiza a busca por documentos<br>
								históricos em outros países e os entrega na sua<br>casa. --}}
							</p>
							<div class="btn-area d-block mt-auto">
								<span class="btn btn-certidao">Saiba mais</span>
							</div>
						</div>
					</div>
				</a>
			</div>

			<div class="col-sm-12 col-md-6 no-pad">
				<a href="{{url('/busca-de-documentos/alemanha-austria-polonia')}}">
					<div class="card box">
						<div class="content">
							<h3 class="title global">Busca de Certidões<br>na Alemanha, Austria e Polônia</h3>
							<p class="description">
								Os Brasil recebeu um grande número de imigrantes dos países germânicos durante o século XIX e XX. A maioria se estabeleceu, criando famílias e dando a origem a diversos descentes no nosso país.
								{{-- A docforce realiza a busca por documentos<br>
								históricos em outros países e os entrega na sua<br>casa. --}}
							</p>
							<div class="btn-area">
								<span class="btn btn-certidao">Saiba mais</span>
							</div>
						</div>
					</div>
				</a>
			</div>

			<div class="col-sm-12 col-md-6 no-pad">
				<a href="{{url('/busca-de-documentos/portugal')}}">
					<div class="card box">
						<div class="content">
							<h3 class="title portugal">Busca de Certidões<br>em Portugal</h3>
							<p class="description">
								Brasileiros descendentes de portugueses podem solicitar a atribuição de cidadania sanguínea para sua família.
								{{-- A docforce realiza a busca por documentos<br>
								históricos em outros países e os entrega na sua<br>casa. --}}
							</p>
							<div class="btn-area">
								<span class="btn btn-certidao">Saiba mais</span>
							</div>
						</div>
					</div>
				</a>
			</div>

			<div class="col-sm-12 col-md-6 no-pad">
				<a href="{{url('/busca-de-documentos/demais-certidoes')}}">
					<div class="card box">
						<div class="content">
							<h3 class="title other">Demais Buscas de<br>Certidões</h3>
							<p class="description">
								A docforce realiza a busca por documentos<br>
								históricos em outros países e os entrega na sua<br>casa.
							</p>
							<div class="btn-area d-block mt-auto">
								<span class="btn btn-certidao">Saiba mais</span>
							</div>
						</div>
					</div>
				</a>
			</div>

		</div>

		

		{{-- @include('site.includes.pagination') --}}
	</div>
</div>

@include('site.includes.cta-faleconosco')

@endsection