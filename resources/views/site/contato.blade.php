@extends('site.layout.main')

@section('title', 'Fale Conosco')
@section('meta-desc', 'Entre em Contato em caso de dúvidas, solicitações ou sugestões.')

@section('breadcrumb')
<li class="breadcrumb-item active" aria-current="page">
	<a href="{{ url('/fale-conosco') }}">@yield('title')</a>
</li>
@endsection

@section('content')

<div class="row contato page" id="contato">
	<div class="container-fluid bg-gray">
		<div class="container">
			<div class="row page-title">
				<div class="container">
					@include('site.includes.breadcrumb')
				</div>
			</div>

			<div class="row content">
				<div class="container">

					<div class="row">
						<div class="col-12 title-area">
							<h1 class="title">
								Contato<br>
								<strong>Fale Conosco</strong>
							</h1>
							<h3 class="subtitle">
								Em caso de dúvidas, solicitações ou sugestões, entre em contato conosco!<br>
								Nosso telefone é <a href="tel:+5511941565963"><b>(11) 94156-5963</b></a> ou se preferir, preencha o formulário abaixo.
							</h3>
						</div>


						<div class="col-lg-8">
                            <form action="{{ route('contato') }}" method="POST" class="form" id="formContato">
                                @csrf

                                @if(session('error'))
                                    <div class="col-11 col-lg-12 alert alert-danger" role="alert">
                                        {{session('error')}}
                                    </div>
                                @endif

                                @if(session('success'))
                                    <div class="box-lightgreen" id="">
                                        <span class="send">{{session('success')}}</span>
                                    </div>
                                @endif

								<div class="form-group">
									<label class="title-input" for="nome_contato">*Nome:</label>
                                    <input type="text" value="" name="nome_contato" id="nome_contato" class="form-control" required="required" />
                                    @if ($errors->has('nome_contato'))
                                        <small class="text-danger" role="alert">
                                            <strong>{{ $errors->first('nome_contato') }}</strong>
                                        </small>
                                    @endif
								</div>

								<div class="form-row">
									<div class="form-group col-12 col-sm-7">
										<label class="title-input" for="email_contato">*E-mail:</label>
                                        <input type="email" value="" name="email_contato" id="email_contato" class="form-control" required="required" placeholder="Ex: mail@exemplo.com.br" />
                                        @if ($errors->has('email_contato'))
                                            <small class="text-danger" role="alert">
                                                <strong>{{ $errors->first('email_contato') }}</strong>
                                            </small>
                                        @endif
									</div>

									<div class="form-group col-12 col-sm-5">
										<label class="title-input" for="telefone_contato">*Telefone:</label>
                                        <input type="text" value="" name="telefone_contato" id="telefone_contato" class="form-control telephone" required="required" placeholder="EX: (00)0000-0000" >
                                        @if ($errors->has('telefone_contato'))
                                            <small class="text-danger" role="alert">
                                                <strong>{{ $errors->first('telefone_contato') }}</strong>
                                            </small>
                                        @endif
									</div>
								</div>

								<div class="form-group">
									<label class="title-input" for="mensagem_contato">*Mensagem:</label>
                                    <textarea name="mensagem_contato" maxlength="480" id="mensagem_contato" cols="30" rows="5" class="form-control" required="required" placeholder="Digite aqui a mensagem que deseja nos passar"></textarea>
                                    @if ($errors->has('mensagem_contato'))
                                        <small class="text-danger" role="alert">
                                            <strong>{{ $errors->first('mensagem_contato') }}</strong>
                                        </small>
                                    @endif
								</div>

								<div class="form-group btns">
									<button type="submit" class="btn btn-default btn-enviar">Enviar Mensagem</button>
								</div>
							</form>
						</div>

						<div class="col-lg-4 contacts">
							<ul class="contact-list">
								<li class="contact telephone">
									<div class="circle">
										<i class="fas fa-phone icon"></i>
									</div>
									<p class="contact-description">
										<b>Contato</b>
										{{-- <br>
										<span class="text-decoration: none!important;">(13) 0000-0000</span> --}}

									</p>
									{{-- Telefone aqui está no after via css --}}
									<a style="text-decoration: none;" href="tel:+5511941565963" target="_blank" title="Entre em contato conosco por telefone"></a>
								</li>

								{{-- <li class="contact whatsapp">
									<div class="circle">
										<i class="fab fa-whatsapp icon"></i>
									</div>
									<p class="contact-description">
										<b>Whatsapp</b><br />
										{{ ('number_formated') }}
									</p>
									<a href="{{ ('url') }}" target="_blank" class="whatsapplink" title="Envie-nos uma mensagem por Whatsapp"></a>
								</li> --}}

								<li class="contact email">
									<div class="circle">
										<i class="far fa-envelope icon"></i>
									</div>
									<p class="contact-description">
										<b>E-mail</b><br />
										contato.docforce@gmail.com
									</p>
									<a href="mailto:contato.docforce@gmail.com" target="_blank" title="Entre em contato conosco por e-mail"></a>
								</li>

								{{-- <li class="contact atendimento">
									<div class="circle">
										<i class="far fa-clock icon"></i>
									</div>
									<p class="contact-description">
										<b>Atendimento</b><br />
										Segunda à Sexta-feira: das 8 às 18hrs
									</p>
								</li> --}}

								{{-- <li class="contact localizacao">
									<div class="circle">
										<i class="fas fa-map-marker-alt icon"></i>
									</div>
									<p class="contact-description">
										<b>Localização</b><br />
										Av. Senador Feijó, 686<br />
										Vila Matias, Santos - SP
									</p>
									<a href="https://goo.gl/maps/stNPM318DPp8ECvU9" target="_blank" title="Clique para visualizar nosso endereço no Google Maps"></a>
								</li> --}}
							</ul>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
</div>

@include('site.includes.cta-faleconosco')

@endsection

@section('js')
{{--  --}}
@endsection
