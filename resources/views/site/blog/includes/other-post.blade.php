@if($posts->isNotEmpty())

    @foreach($posts as $post)
    
    <a href="{{ route('site.blog.integra', ['name' => $post->post_name]) }}">
        <div class="post">
            <div class="card">
                @if ($post->thumbnail !== null)
                <?php
                    $thumbnail = $post->thumbnail->size('post-thumb-wide')
                ?>
                <img src="{{ $thumbnail }}" alt="{{ $post->title}}" class="card-img-top">
                @else

                @endif
                <div class="card-body">
                    <p class="card-text">{{ $post->title }}</p>
                </div>
                <div class="btn-area">
                    <span class="btn btn-readmore"><i class="fas plus fa-plus"></i>Leia mais</span>
                </div>
                </div>
        </div>
    </a>

    @endforeach

@endif