@if($posts->isNotEmpty())

    @foreach($posts as $post)
    <a href="{{ route('site.blog.integra', ['name' => $post->post_name]) }}">
    <div class="card">
        <div class="card-body">
            @if ($post->thumbnail !== null)
            <?php
                $thumbnail = $post->thumbnail->size('post-thumb-wide')
            ?>
            <div class="card-img">
            <img src="{{ $thumbnail }}" alt="{{ $post->title }}" class="img-fluid img">
            </div>
            @else
            <p class="d-flex align-items-center mx-auto">Imagem Indisponível</p>
            @endif


            <div class="card-content">
                <span class="card-date"><i class="far calendar fa-calendar-alt"></i>{{ $post->post_date }}</span>

                <h2 class="card-title">{{ $post->title }}</h2>
                
                <p class="card-text">
                    {{ strip_tags(substr($post->content, 0, 200))}}
                </p>

                
                <div class="btn-area">
                    <span class="btn btn-readmore"><i class="fas plus fa-plus"></i>Leia mais</span>
                </div>
            </div>
            
        </div>
    </div>
    </a>
    @endforeach
 
@endif