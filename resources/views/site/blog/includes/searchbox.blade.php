<form action="/blog" type="GET">
    <span class="search-box"> 
        <input type="text" name="search" class="search-input" placeholder="Buscar" value="{{ (isset($_GET['search']) && !empty($_GET['search']))? $_GET['search'] : '' }}" required="required" tabindex="1">
        <button type="submit" class="search-icon" tabindex="2"><i class="fa fa-search"></i></button>
    </span>
</form>