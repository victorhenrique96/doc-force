@extends('site.layout.main')

@section('title', 'Blog')
@section('meta-desc', 'Blog Docforce')

@section('breadcrumb')
<li class="breadcrumb-item active" aria-current="page">
	<a href="{{ url('/blog') }}">Blog</a>
</li>
@endsection

@section('content')



<div class="row blog page" id="Blog">
	<div class="container-fluid bg-gray-blog">
		<div class="container">
			<div class="row page-title">
				<div class="container">
					@include('site.includes.breadcrumb')
				</div>
			</div>
		</div>
	</div>

	<div class="container blog">
		<div class="row pb-5 integra">
			<div class="col-sm-12 col-md-12 col-lg-12">
                <div class="conteudo">
					<h3 class="title-post">
                        <span class="card-date"><i class="far calendar fa-calendar-alt"></i>{{ $post->post_date }}</span>
						<strong>{{ $post->title }}</strong>
					</h3>
				</div>
            </div>

            <div class="container row post-integra">
                <div class="col-sm-12 col-lg-8">
                    <div class="post">
                        <div class="post-body">

                            @if($post->thumbnail !== null)
                            <?php
                                $thumbnail = $post->thumbnail->size('post-featured');
                                $thumbnail = (is_array($thumbnail))? $thumbnail = $thumbnail['url'] : $thumbnail;
                            ?>
                            <img src="{{ $thumbnail }}" alt="{{ $post->title }}" class="img-fluid">
                            @else
                                <p class="d-flex h-50 align-items-center mx-auto">Imagem Indisponível</p>
                            @endif

                            <div class="post-content">
                                <p class="post-text">
                                    {{ strip_tags($post->post_content)}}
                                </p>
                            </div>

                            <div class="share-post">
                                <div class="fb-share-button" data-href="https://developers.facebook.com/docs/plugins/" data-layout="button_count" data-size="large"><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&amp;src=sdkpreparse" class="fb-xfbml-parse-ignore">Compartilhar</a></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-12 col-lg-3 offset-1 other-posts">
                    <div class="title">
                        <i class="far icon fa-file-alt"></i>
                        <span>Outras Notícias</span>
                    </div>

                    @include('site.blog.includes.other-post', ['posts' => $other_posts])

                </div>

            </div>
		</div>
	</div>
</div>
		
@include('site.includes.cta-faleconosco')

@endsection