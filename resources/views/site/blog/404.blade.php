@extends('site.layout.main')

@section('title', 'Blog')
@section('meta-desc', 'Blog Docforce')

@section('breadcrumb')
<li class="breadcrumb-item active" aria-current="page">
	<a href="{{ url('/blog') }}">@yield('title')</a>
</li>
@endsection

@section('content')

<div class="row blog page" id="Blog">
	<div class="container-fluid bg-gray-blog">
		<div class="container">
			<div class="row page-title">
				<div class="container">
					@include('site.includes.breadcrumb')
				</div>
			</div>
		</div>
	</div>

	<div class="container blog">
		<div class="row pb-5">
			<div class="col-sm-12 col-md-12 col-lg-8">
                <div class="conteudo">
					<h3 class="title-blog">
                        Blog<br>
						<strong>Dicas e Notícias</strong>
					</h3>
				</div>
            </div>

            <div class="col-sm-12 col-md-12 col-lg-4">
                @include('site.blog.includes.searchbox')
            </div>

            <div class="container listagem-posts">


					<div class="container blog-404">
						<div class="row justify-content-center">
							<p class="h1">Essa página não existe!</h1>
								<br>
								<br>
								<br>
						</div>
						<div class="row justify-content-center">
							<div class="btn-area">
								<a href="{{url('/blog')}}" class="btn btn-default">Clique aqui para retornar ao Blog</a>
							</div>
						</div>
					</div>
				
            </div>
		</div>
	</div>
</div>


@include('site.includes.cta-faleconosco')

@endsection