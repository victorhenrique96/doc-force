@extends('site.layout.main')

@section('title', 'Pesquisas Genealógicas')
@section('meta-desc', 'Saiba mais sobre a Docforce.')

@section('breadcrumb')
<li class="breadcrumb-item active" aria-current="page">
	<a href="{{ url('/pesquisas-genealogicas') }}">@yield('title')</a>
</li>
@endsection

@section('content')

<div class="row about page" id="Research">
	<div class="container-fluid bg-gray">
		<div class="container">
			<div class="row page-title">
				<div class="container">
					@include('site.includes.breadcrumb')
				</div>
			</div>
		</div>
	</div>

	<div class="container">
		<div class="row pb-5">
			<div class="col-sm-12 col-md-12 col-lg-5">
				<img class="img-fluid" src="{{asset('images/pesquisas/pesquisas-genealogicas.png')}}">
			</div>

			
			<div class="col-sm-12 col-md-12 col-lg-7">
				<div class="conteudo">
					<h3 class="title-research">
						<strong>Pesquisas<br>Genealógicas</strong>
					</h3>

					<h4 class="description-research">Conheça seus antepassados e saiba a partir da pesquisa<br>
						genealógica se é possível obter sua dupla cidadania estrangeira.</h4>

					<h5 class="origins-research">
						<strong>Resgate as origens de suas famílias</strong>
					</h5>
				</div>
			</div>
			<div class="row research-paragraph">
				<div class="col-sm-12 col-12">
					<p class="paragraph-content">A reconstrução da própria história familiar é um processo que visa preencher as lacunas documentais e de memória familiar. Conhecer as próprias origens é mais do que obter informações familiares incompletas, mas sim um encontro com a origem de antepassados, entender sua essência e descobrir o mix cultural rico na origem das famílias brasileiras.</p>
					<div class="w-100"></div>
					<p class="paragraph-content">
					A docforce possui pacotes para pesquisa de duas, três ou quatro gerações da sua família, informando nome completo, local e data de nascimento e casamento de seus parentes.</p>
				</div>
				<div class="btn-area">
				<a href="{{url('/fale-conosco')}}" title="Clique aqui e confira" class="btn btn-clickhere confira">Clique aqui e confira</a>
				</div>
			</div>
		</div>
	</div>

	
</div>


@include('site.includes.cta-faleconosco')

@endsection

@section('js')
<script type="text/javascript">
	// start.plugins.swiper.sliderBrands();
</script>
@endsection
