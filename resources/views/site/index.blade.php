@extends('site.layout.main')

@section('title', 'Busca de Documentos e Pesquisas Genealógicas')

@section('content')

	<div class="row banner">
		@include('site.includes.banner')
	</div>

	<div class="row services" id="servicos">
		@include('site.includes.services')
	</div>

	@include('site.includes.cta-faleconosco')

	<div class="row main-about about" id="quemSomos">
		@include('site.includes.about')
	</div>

<?php
/*
	<div class="row main-about" id="quemSomos">
		@include('site.includes.main-about')
	</div>

	<div class="row main-contato" id="contato">
		@include('site.includes.main-contato')
	</div>
*/
?>
@endsection

@section('js')

@endsection
