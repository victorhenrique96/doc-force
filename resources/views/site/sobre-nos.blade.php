@extends('site.layout.main')

@section('title', 'Sobre nós')
@section('meta-desc', 'Saiba mais sobre a Docforce.')

@section('breadcrumb')
<li class="breadcrumb-item active" aria-current="page">
	<a href="{{ url('/sobre-nos') }}">@yield('title')</a>
</li>
@endsection

@section('content')

<div class="row about page bg-about" id="quemSomos">
	<div class="container">
		<div class="row page-title">
			<div class="container">
				@include('site.includes.breadcrumb')
			</div>
		</div>
	</div>

	<div class="col-12">
		<div class="row pb-5">
			<div class="col-sm-5 about-image-integra">
			</div>

			<div class="col-sm-6 content">
				<div class="title-area">
					<h3 class="title-integra">
						<strong>Sobre nós</strong>
					</h3>
					<h4 class="about-description">
						A DocForce é uma empresa especializada no resgate da sua origem.<br>
						Buscamos documentos históricos e pesquisamos genealogia. 
					</h4>
				</div>

					<ul class="about-list">
						<li class="list-item">
							Resgatamos documentos históricos e a memória familiar;
						</li>
						<li class="list-item">
							Buscamos documentos em outros países e entregamos na sua casa;
						</li>
						<li class="list-item">
							Criamos a arvore genealógica da sua família;
						</li>
						<li class="list-item">
							Auxiliamos com suas certidões para o seu processo de dupla cidadania;
						</li>
						
						<div class="btn-area">
							<a href="{{ url('/contato') }}" class="btn btn-default fale-conosco">Fale conosco agora mesmo!</a>
						</div>
					</ul>
			</div>
		</div>
	</div>
</div>

<div class="container">
	<div class="row about">
		<div class="col-12 parceiros">
			<h5 class="title">Parcerias de Sucesso</h5>
		</div>
		<div class="col-6 logo">
			<a href="https://rotunnocidadania.com.br/" target="_blank"><img src="{{asset('images/parceiros/logo-rotunno.png')}}" class="img-fluid" alt=""></a>
		</div>
		<div class="col-6 logo">
			<a href="https://www.4doc.net/" target="_blank">
			<img src="{{asset('images/parceiros/logo-4doc.png')}}" class="img-fluid" alt=""></a>
		</div>
	</div>
</div> 


@include('site.includes.cta-faleconosco')

@endsection

@section('js')
<script type="text/javascript">
	// start.plugins.swiper.sliderBrands();
</script>
@endsection
