<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
	@include('site.includes.header.head')
</head>
<body>
	@include('site.includes.header.header')


	<main class="container-fluid main-wrapper">
		@yield('content')

		@if(Request::segment(1) != 'fale-conosco')

		<div class="btn btn-default fale-conosco closed" id="faleConosco">
			<span class="whatsapp"></span>
			<p>
				Fale Conosco por WhatsApp<br/>
				<strong>(11) 94156-5963</strong>
			</p>
			<a style="text-decoration:none!important;" href="https://api.whatsapp.com/send?phone=5511941565963&text=Ol%C3%A1%20Docforce!" target="_blank" title="Fale conosco por WhatsApp"></a>
		</div>

		@endif

	</main>

	@include('site.includes.footer.footer')

	<!-- scripts -->
	@include('site.includes.footer.scripts')
</body>
</html>