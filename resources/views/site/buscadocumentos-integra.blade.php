@extends('site.layout.main')

@section('meta-desc', 'Saiba mais sobre a Docforce.')

@section('breadcrumb')
<li class="breadcrumb-item active" aria-current="page">
	<a href="{{ url('/busca-de-documentos') }}">@yield('title')</a>
</li>
@endsection

@section('content')

@if(Request::segment(2) == 'brasil')
	@section('title', 'Busca de Certidões no Brasil')
	@include('site.includes.documentos.certidao-brasil')
@endif

@if(Request::segment(2) == 'espanha')
	@section('title', 'Busca de Certidões na Espanha')
	@include('site.includes.documentos.certidao-espanha')
@endif

@if(Request::segment(2) == 'italia')
	@section('title', 'Busca de Certidões na Itália')
	@include('site.includes.documentos.certidao-italia')
@endif

@if(Request::segment(2) == 'alemanha-austria-polonia')
	@section('title', 'Busca de Certidões na Alemanha, Áustria e Polônia')
	@include('site.includes.documentos.certidao-alemanha')
@endif


@if(Request::segment(2) == 'portugal')
	@section('title', 'Busca de Certidões em Portugal')
	@include('site.includes.documentos.certidao-portugal')
@endif

@if(Request::segment(2) == 'demais-certidoes')
	@section('title', 'Demais Buscas de Certidões')
	@include('site.includes.documentos.demais-certidoes')
@endif


@include('site.includes.cta-faleconosco')

@endsection

@section('js')
<script type="text/javascript">
	// start.plugins.swiper.sliderBrands();
</script>
@endsection
