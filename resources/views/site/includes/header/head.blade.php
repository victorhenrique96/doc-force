<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no">
<meta name="csrf-token" content="{{ csrf_token() }}" />

<?php
/*
@if(Request::path() === "/institucionais/modelo-2" || Request::path() === "institucionais/modelo-2" || Request::path() === "institucionais/modelo-2/" || Request::path() === "/")
*/
?>
{{-- @if(isHome())
    <title>@yield('title')</title>
@else --}}
    <title>@yield('title') | Doc Force</title>
{{-- @endif --}}

<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous" />

<link rel="stylesheet" href="{{ asset('/css/vendor/bootstrap.min.css') }}" type="text/css" />

<link rel="stylesheet" href="{{ asset('/css/main.css') }}" type="text/css" />

@yield('css')


@include('site.includes.header.metatags')

@include('.site.includes.header.favicon')
