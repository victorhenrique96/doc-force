{{-- @if(isHome()) --}}
    {{-- <header class="nav-wrapper" id="topo">
@else --}}
    <header class="nav-wrapper inside" id="topo">
{{-- @endif --}}

	<div class="container">
		<div class="row">
			<div class="col-3 col-lg-2 col-logo">
				<a href="{{ url('/') }}" title="Docforce">
					<img src="{{ asset('/images/logo-topo.png') }}" class="img-fluid" alt="Docforce" />
				</a>
			</div>

			<div class="col-3 d-lg-none btn-menu">
				<button class="navbar-toggler btn btn-circle-gold collapsed" type="button" data-toggle="collapse" data-target="#menuLinks" aria-controls="menuLinks" aria-expanded="false">
					<i class="fas fa-bars icon"></i>
				</button>
			</div>

			<nav class="col-12 col-lg-10 navbar navbar-expand-lg">
				<div class="collapse navbar-collapse" id="menuLinks">
					<ul class="navbar-nav">
						<li class="nav-item active">
							<a href="{{ url('/sobre-nos') }}" class="nav-link" title="">Sobre nós</a>
						</li>

						<li class="nav-item">
							<a href="{{ url('/pesquisas-genealogicas') }}" class="nav-link anchor">Pesquisas Genealógicas</a>
						</li>

						<li class="nav-item dropdown">
							<a class="nav-link dropdown-toggle d-lg-none" href="{{ url('/busca-de-documentos') }}" id="BuscaDocumentos" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Busca de documentos</a>
							<a class="nav-link dropdown-toggle d-none d-lg-block" href="{{ url('/busca-de-documentos') }}" id="BuscaDocumentos" >Busca de documentos</a>
							<div class="dropdown-menu" aria-labelledby="BuscaDocumentos">
								<a class="nav-link dropdown-item" href="{{ url('/busca-de-documentos/brasil') }}">Busca de certidões<br> no Brasil</a>
								<a class="nav-link dropdown-item" href="{{ url('/busca-de-documentos/espanha') }}">Busca de certidões<br> na Espanha</a>
								<a class="nav-link dropdown-item" href="{{ url('/busca-de-documentos/italia') }}">Busca de certidões<br> na Itália</a>
								<a class="nav-link dropdown-item" href="{{ url('/busca-de-documentos/alemanha-austria-polonia') }}">Busca de certidões<br> na Alemanha, Áustria<br> e Polônia</a>
								<a class="nav-link dropdown-item" href="{{ url('/busca-de-documentos/portugal') }}">Busca de certidões<br> em Portugal</a>
								<a class="nav-link dropdown-item" href="{{ url('/busca-de-documentos/demais-certidoes') }}">Demais buscas<br> de certidões</a>
							</div>
						</li>

						<li class="nav-item">
							<a href="{{ url('/blog') }}" class="nav-link anchor">Blog</a>
						</li>

						<li class="nav-item">
							<a href="{{ url('/fale-conosco') }}" class="nav-link anchor">Fale Conosco</a>
						</li>


						<li class="nav-item social-media">
							<div class="telephone">
								<a href="tel:+5511941565963" class="nav-link" target="_blank" title="Entre em contato conosco por telefone">
									<i class="fas fa-phone icon"></i>
								</a>
							</div>

							<div class="facebook">
								<a href="https://www.facebook.com/DocForce.BR/" class="nav-link" target="_blank" title="Curta a nossa página no Facebook">
									<i class="fab fa-facebook-f icon"></i>
								</a>
							</div>

							<div class="instagram">
								<a href="https://www.instagram.com/docforce.br/" target="_blank" class="nav-link" title="Siga-nos no Instagram">
									<i class="fab fa-instagram icon"></i>
								</a>
							</div>

							{{-- <div class="twitter">
								<a href="https://www.twitter.com/" class="nav-link" target="_blank" title="Siga-nos no Twitter">
									<i class="fab fa-twitter icon"></i>
								</a>
							</div> --}}
						</li>
					</ul>
				</div>
			</nav>
		</div>
	</div>
</header>