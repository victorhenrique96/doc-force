<!-- for Google -->
<meta name="description" content="@yield('meta-desc', '')" />
<meta name="company" content="" />
<meta name="author" content="KBRTEC" />
<meta name="copyright" content="" />
<meta name="application-name" content="" />
<meta name="distribution" content="Global" />
<meta name="rating" content="General" />

<!-- Facebook -->
<meta property="og:type" content="website">
<meta property="og:url" content="{{ url('/') }}@yield('meta-url', '')">
<meta property="og:title" content="@yield('meta-title', '')">
<meta property="og:description" content="@yield('meta-desc', '')">
<meta property="og:site_name" content="@yield('site-name', '')">
<meta property="og:image" content="@yield('meta-image', asset('/images/logo-doc.jpg'))">

<!-- Twitter -->
<meta name="twitter:card" content="summary" />
<meta name="twitter:title" content="@yield('meta-title', '')" />
<meta property="twitter:description" content="@yield('meta-desc', '')">
<meta property="twitter:image" content="@yield('meta-image', asset('/images/logo-doc.jpg'))">
<!-- /Share metatags goes here -->