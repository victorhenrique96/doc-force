<div class="row about page" id="Research">
	<div class="container-fluid bg-gray">
		<div class="container">
			<div class="row page-title">
				<div class="container">
					@include('site.includes.breadcrumb')
				</div>
			</div>
		</div>
	</div>

	<div class="container">
		<div class="row pb-5">
			<div class="col-sm-5 col-md-12 col-lg-5">
				<img class="img-fluid" src="{{asset('images/documentos/alemanha-austria-polonia.jpg')}}">
			</div>

			
			<div class="col-sm-7 col-md-12 col-lg-7">
				<div class="conteudo">
					<h3 class="title-certidao">
						<strong>Buscas de Certidões<br>na Alemanha, Áustria e Polônia</strong>
					</h3>

					<h4 class="description-research mt-5">Os Brasil recebeu um grande número de imigrantes dos países germânicos durante o século XIX e XX. A maioria se estabeleceu, criando famílias e dando a origem a diversos descentes no nosso país.<br>

					<br>
					
					</h4>

					<h5 class="origins-research">
						<strong>Confira abaixo os tipos de serviços<br>prestados pela docforce</strong>
					</h5>
				</div>
			</div>
			<div class="row margin-fix">
				<div class="card-deck certidoes-deck">

					<div class="card">
					  <div class="card-body">
						<div class="card-title">
							<p class="title">
								Certidão Atualizada – 2ª Via
							</p>
							<p class="desc">
								Taufaufzeichnung / Heiratsurkunde.
							</p>
						</div>
						<div class="card-text box-gray">
							<strong>
								Serviço emitido quando o cliente possui cópia digitalizada do documento original;
							</strong>
						</div>
						<div class="value">
							<span class="investment">Valor do investimento:</span>
							<strong>R$ 990,00</strong>
						</div>
						<div class="btn-area">
							<a href="{{url('/fale-conosco')}}" class="btn btn-clickhere confira">Quero Contratar</a>
						</div>
					  </div>
					</div>

					<div class="card">
						<div class="card-body">
						  <div class="card-title">
							  <p class="title">
								Pesquisa
							  </p>
							  <p class="desc">
								Taufaufzeichnung / Heiratsurkunde.
							  </p>
						  </div>
						  <div class="card-text box-gray">
							  <strong>
								Serviço realizado quando as informações referente ao ancestral são apenas superficiais;
							  </strong>
						  </div>
						  <div class="value">
							  <span class="investment">Valor do investimento:</span>
							  <strong>R$ 1.490,00</strong>
						  </div>
						  <div class="btn-area">
							  <a href="{{url('/fale-conosco')}}" class="btn btn-clickhere confira">Quero Contratar</a>
						  </div>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
</div>