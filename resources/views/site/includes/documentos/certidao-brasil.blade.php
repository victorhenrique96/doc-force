<div class="row about page" id="Research">
	<div class="container-fluid bg-gray">
		<div class="container">
			<div class="row page-title">
				<div class="container">
					@include('site.includes.breadcrumb')
				</div>
			</div>
		</div>
	</div>

	<div class="container">
		<div class="row pb-5">
			<div class="col-sm-5 col-md-12 col-lg-5">
				<img class="img-fluid" src="{{asset('images/documentos/brasil.jpg')}}">
			</div>

			
			<div class="col-sm-7 col-md-12 col-lg-7">
				<div class="conteudo">
					<h3 class="title-certidao">
						<strong>Buscas de<br>Certidões no Brasil</strong>
					</h3>

					<h4 class="description-research mt-5">As certidões e registros históricos no Brasil dos seus antepassados, que não estão no acervo da família, podem ser recuperadas com nossa equipe.<br>

						<br>

						Através da expertise de buscas em acervos históricos públicos e órgãos religiosos, conseguimos recuperar certidões de nascimento e casamento que estejam suprimidas na árvore genealógica.<br>
						</h4>

					<h5 class="origins-research">
						<strong>Confira abaixo os tipos de serviços<br>prestados pela docforce</strong>
					</h5>
				</div>
			</div>
			<div class="row margin-fix">
				<div class="card-deck certidoes-deck">

					<div class="card">
					  <div class="card-body">
						<div class="card-title">
							<p class="title">
								Busca Simples
							</p>
							<p class="desc">
								Certidão de Nascimento/Batismo e Certidão de Casamento.
							</p>
						</div>
						<div class="card-text box-gray">
							<strong>
								Serviço realizado quando possui-se informações aproximadas referente a certidão.
							</strong>
						</div>
						{{-- <div class="value">
							<span class="investment">Valor do investimento:</span>
							<strong>R$ 990,00</strong>
						</div> --}}
						<div class="btn-area">
							<a href="{{url('/fale-conosco')}}" class="btn btn-clickhere confira">Entre em contato<br>para obter um orçamento</a>
						</div>
					  </div>
					</div>

					<div class="card">
						<div class="card-body">
						  <div class="card-title">
							  <p class="title">
								Busca Complexa
							  </p>
							  <p class="desc">
								  Certidão de Nascimento/Batismo e Certidão de Casamento.
							  </p>
						  </div>
						  <div class="card-text box-gray">
							  <strong>
								Serviço realizado quando as informações referente ao ancestral são apenas superficiais.
							  </strong>
						  </div>
						  {{-- <div class="value">
							  <span class="investment">Valor do investimento:</span>
							  <strong>R$ 990,00</strong>
						  </div> --}}
						  <div class="btn-area">
							  <a href="{{url('/fale-conosco')}}" class="btn btn-clickhere confira">Entre em contato<br>para obter um orçamento</a>
						  </div>
						</div>
					</div>

					{{-- <div class="card">
						<div class="card-body">
						  <div class="card-title">
							  <p class="title">
								  Pesquisa
							  </p>
							  <p class="desc">
								  atto di nascita ou atto di matrimonio.
							  </p>
						  </div>
						  <div class="card-text box-gray">
							  <strong>
								Serviço realizado quando o cliente possui algumas as informações da certidão, mas não possui a cópia do documento original;
							  </strong>
						  </div>
						  <div class="value">
							  <span class="investment"></span>
							  <strong></strong>
						  </div>
						  <div class="btn-area">
							  <a href="#" class="btn btn-clickhere confira">Quero Contratar</a>
						  </div>
						</div>
					</div> --}}

				</div>
			</div>
		</div>
	</div>
</div>