<div class="row about page" id="Research">
	<div class="container-fluid bg-gray">
		<div class="container">
			<div class="row page-title">
				<div class="container">
					@include('site.includes.breadcrumb')
				</div>
			</div>
		</div>
	</div>

	<div class="container">
		<div class="row pb-5">
			<div class="col-sm-5 col-md-12 col-lg-5">
				<img class="img-fluid" src="{{asset('images/documentos/espanha.jpg')}}">
			</div>

			
			<div class="col-sm-7 col-md-12 col-lg-7">
				<div class="conteudo">
					<h3 class="title-certidao">
						<strong>Buscas de<br>Certidões na Espanha</strong>
					</h3>

					<h4 class="description-research mt-5">Devido a imigração europeia que ocorreu entre os séculos XIX e XX, estima-se que aproximadamente 15 milhões de brasileiros tenham descendência espanhola.<br>

						<br>
						

						Quer descobrir a origem da sua família espanhola ou obter um certificado atualizado dos seus ancestrais.<br>
						</h4>

					<h5 class="origins-research">
						<strong class="text-center">Conheça abaixo as soluções<br> da docforce</strong>
					</h5>
				</div>
			</div>
			<div class="row margin-fix">
				<div class="card-deck certidoes-deck">

					<div class="card">
					  <div class="card-body">
						<div class="card-title">
							<p class="title">
								Certidão Atualizada – 2ª Via
							</p>
							<p class="desc">
								Certificado de nacimiento / Certificado de matrimonio.
							</p>
						</div>
						<div class="card-text box-gray">
							<strong>
								Serviço emitido quando o cliente possui cópia digitalizada do documento original;
							</strong>
						</div>
						<div class="value">
							<span class="investment">Valor do investimento:</span>
							<strong>R$ 990,00</strong>
						</div>
						<div class="btn-area">
							<a href="{{url('/fale-conosco')}}" class="btn btn-clickhere confira">Quero Contratar</a>
						</div>
					  </div>
					</div>

					<div class="card">
						<div class="card-body">
						  <div class="card-title">
							  <p class="title">
								  Busca Simples
							  </p>
							  <p class="desc">
								Certificado de nacimiento / Certificado de matrimonio.
							  </p>
						  </div>
						  <div class="card-text box-gray">
							  <strong>
								Serviço realizado quando possui-se informações aproximadas referente a certidão;
							  </strong>
						  </div>
						  <div class="value">
							  <span class="investment">Valor do investimento:</span>
							  <strong>R$ 1.490,00</strong>
						  </div>
						  <div class="btn-area">
							  <a href="{{url('/fale-conosco')}}" class="btn btn-clickhere confira">Quero Contratar</a>
						  </div>
						</div>
					</div>

					<div class="card">
						<div class="card-body">
						  <div class="card-title">
							  <p class="title">
								Pesquisa
							  </p>
							  <p class="desc">
								Certificado de nacimiento / Certificado de matrimonio.
							  </p>
						  </div>
						  <div class="card-text box-gray">
							  <strong>
								Serviço realizado quando as informações referente ao ancestral são apenas superficiais;
							  </strong>
						  </div>
						  {{-- <div class="value">
							  <span class="investment">Valor do investimento:</span>
							  <strong>R$ 990,00</strong>
						  </div> --}}
						  <div class="btn-area">
							  <a href="{{url('/fale-conosco')}}" class="btn btn-clickhere confira">Quero Contratar</a>
						  </div>
						</div>
					  </div>

					</div>
			</div>
		</div>
	</div>
</div>