<div class="row about page" id="Research">
	<div class="container-fluid bg-gray">
		<div class="container">
			<div class="row page-title">
				<div class="container">
					@include('site.includes.breadcrumb')
				</div>
			</div>
		</div>
	</div>

	<div class="container">
		<div class="row pb-5">
			<div class="col-sm-5 col-md-12 col-lg-5">
				<img class="img-fluid" src="{{asset('images/documentos/italia.png')}}">
			</div>

			
			<div class="col-sm-7 col-md-12 col-lg-7">
				<div class="conteudo">
					<h3 class="title-certidao">
						<strong>Buscas de<br>Certidões na Itália</strong>
					</h3>

					<h4 class="description-research mt-5">A Itália é um dos países mais procurados pelos brasileiros para o reconhecimento da dupla cidadania.<br>

						<br>
						
						Estima que 28 milhões de brasileiros possuem direito a cidadania italiana.<br>
						
						<br>

						O primeiro passo para iniciar esse processo é obter a certidão de nascimento e casamento do italiano que migrou para o Brasil.<br>
						</h4>

					<h5 class="origins-research">
						<strong>Confira abaixo os tipos de serviços<br>prestados pela docforce</strong>
					</h5>
				</div>
			</div>
			<div class="row margin-fix">
				<div class="card-deck certidoes-deck">

					<div class="card">
					  <div class="card-body">
						<div class="card-title">
							<p class="title">
								Certidão Atualizada – 2ª Via
							</p>
							<p class="desc">
								atto di nascita / atto di matrimonio.
							</p>
						</div>
						<div class="card-text box-gray">
							<strong>
							Serviço emitido quando o cliente possui
							cópia digitalizada do documento
							original;
							</strong>
						</div>
						<div class="value">
							<span class="investment">Valor do investimento:</span>
							<strong>R$ 990,00</strong>
						</div>
						<div class="btn-area">
							<a href="{{url('/fale-conosco')}}" class="btn btn-clickhere confira">Quero Contratar</a>
						</div>
					  </div>
					</div>

					<div class="card">
						<div class="card-body">
						  <div class="card-title">
							  <p class="title">
								  Certidão Atualizada – 2ª Via
							  </p>
							  <p class="desc">
								  atto di nascita / atto di matrimonio.
							  </p>
						  </div>
						  <div class="card-text box-gray">
							  <strong>
							  Serviço emitido quando o cliente possui
							  cópia digitalizada do documento
							  original;
							  </strong>
						  </div>
						  <div class="value">
							  <span class="investment">Valor do investimento:</span>
							  <strong>R$ 1.490,00</strong>
						  </div>
						  <div class="btn-area">
							  <a href="{{url('/fale-conosco')}}" class="btn btn-clickhere confira">Quero Contratar</a>
						  </div>
						</div>
					</div>

					<div class="card">
						<div class="card-body">
						  <div class="card-title">
							  <p class="title">
								  Certidão Atualizada – 2ª Via
							  </p>
							  <p class="desc">
								  atto di nascita / atto di matrimonio.
							  </p>
						  </div>
						  <div class="card-text box-gray">
							  <strong>
							  Serviço emitido quando o cliente possui
							  cópia digitalizada do documento
							  original;
							  </strong>
						  </div>
						  <div class="value">
							  <span class="investment">Valor do investimento:</span>
							  <strong>R$ 990,00</strong>
						  </div>
						  <div class="btn-area">
							  <a href="{{url('/fale-conosco')}}" class="btn btn-clickhere confira">Quero Contratar</a>
						  </div>
						</div>
					  </div>

					</div>
			</div>
		</div>
	</div>
</div>