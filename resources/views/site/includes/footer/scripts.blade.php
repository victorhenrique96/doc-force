<div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v6.0"></script>
<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
<script type="application/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js"></script>
<script type="application/javascript" src="{{ asset('/js/vendor/vendor.min.js') }}"></script>
<script type="application/javascript" src="{{ asset('/js/main.min.js') }}"></script>
{{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.1/js/all.min.js" data-search-pseudo-elements></script> --}}

@yield('js')
