<footer class="container-fluid">
	<div class="row top-footer">
		<div class="container">

			<div class="row">
				<div class="col-md-12 col-lg-5 logo-social">
					<a href="{{ url('/') }}" class="link-logo">
						<img src="{{ asset('/images/logo-footer.png') }}" class="img-fluid" alt="Docforce" />
					</a>

					<ul class="social-media">
						<li>
							<a href="https://www.facebook.com/DocForce.BR/" target="_blank" class="facebook" title="Curta a nossa página no Facebook">
								<i class="fab fa-facebook-f icon"></i>
							</a>
						</li>
						<li>
							<a href="https://www.instagram.com/docforce.br/" target="_blank" class="instagram">
								<i class="fab fa-instagram icon"></i>
							</a>
						</li>
						{{-- <li>
							<a href="https://twitter.com/" target="_blank" class="twitter" title="Siga-nos no Twitter!">
								<i class="fab fa-twitter icon"></i>
							</a>
						</li> --}}

						{{-- <li>
							<a href="https://pt.linkedin.com/" target="_blank" class="linkedin" title="Visualize nosso perfil no Linkedin">
								<i class="fab fa-linkedin-in icon"></i>
							</a>
						</li> --}}
					</ul>
				</div>

				<div class="col-md-12 col-lg-4 acesso-rapido">
					<h6 class="title">Acesso</h6>
					<ul class="footer-links">
						<li>
							<a href="{{ url('/sobre-nos') }}">Sobre nós</a>
						</li>
						<li>
							<a href="{{ url('/pesquisas-genealogicas') }}">Pesquisas Genealógicas</a>
						</li>
						<li>
							<a href="{{ url('/busca-de-documentos') }}">Busca de Documentos</a>
						</li>
						<li>
							<a href="{{ url('/blog') }}">Blog</a>
						</li>
						<li>
							<a href="{{ url('/fale-conosco') }}">Fale Conosco</a>
						</li>
					</ul>
				</div>

				<div class="col-md-12 col-lg-3 contato">
					<h6 class="title">Contatos</h6>
					<ul class="contatos">
						<li>
							<a href="tel:+5511941565963" target="_blank" class="telefone" title="Entre em contato conosco por telefone">
								<i class="fas fa-mobile-alt icon"></i>
								(11) 94156-5963
							</a>
						</li>
						<li>
							<a href="mailto:contato.docforce@gmail.com" target="_blank" class="email" title="Entre em contato conosco por e-mail">
								<i class="fas fa-envelope icon"></i>
								contato.docforce@gmail.com
							</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>

	<div class="row bottom-footer">
		<div class="container">
			<div class="row">
				<div class="col-12 col-sm-6 copyright">
					<p>
						<a href="{{ url('/') }}" title="Docforce">Docforce</a> - Todos os direitos reservados.
					</p>
				</div>

				<div class="col-12 col-sm-6 developed-by">
					<p>
						<a href="http://kbrtec.com.br" target="_blank" title="Acesse o site da KBRTEC">
							Desenvolvido por <img src="{{ asset('/images/logo-kbrtec-w.png') }}" alt="KBRTEC Soluções Online" />
						</a>
					</p>
				</div>
			</div>
		</div>
	</div>
</footer>