<div id="main-banner" class="carousel slide carousel-fade" data-ride="carousel" data-interval="false">
<!-- <div id="main-banner" class="carousel slide carousel-fade" data-ride="carousel"> -->
	<div class="carousel-inner">

		{{-- Slide 1 --}}
		<div class="carousel-item active slide1">
			<img src="{{ asset('/images/banner/slide1.jpg') }}" class="d-block w-100" alt="">
			<div class="container carousel-caption">
				<h2 class="title">
					Pesquisa<br>
					genealógica<br>
					e documentos<br>
					históricos
				</h2>
				<div class="btn-area">
				<a href="{{url('/sobre-nos')}}" title="Saiba mais" class="btn btn-arrow saiba-mais">Saiba mais</a>
				</div>
			</div>
		</div>
		{{-- Slide 1 --}}

		{{-- Slide 2 --}}
		<div class="carousel-item slide2">
			<img src="{{ asset('/images/banner/slide2.jpg') }}" class="d-block w-100" alt="" />

			<div class="container carousel-caption">
				<div class="row">
					<div class="col-md-12 col-lg-4">
						<h2 class="title">
							Busca de documentos históricos na Europa
						</h2>
					</div>
					<div class="col-md-12 col-lg-8">
						<div class="box">
							<h3 class="subtitle">
								Pesquisamos e encontramos documentos na Itália, Portugal, Espanha, Alemanha, Áustria e Polônia.
							</h3>
						</div>
					</div>
				</div>

				<div class="btn-area">
				<a href="{{url('/busca-de-documentos')}}" title="Saiba mais" class="btn btn-arrow saiba-mais">Saiba mais</a>
				</div>
			</div>
		</div>
		{{-- Slide 2 --}}

		{{-- Slide 3 --}}
		<div class="carousel-item slide3">
			<img src="{{ asset('/images/banner/slide3.jpg') }}" class="d-block w-100" alt="" />

			<div class="container carousel-caption">
				<div class="row">
					<div class="col-md-12 col-lg-4">
						<h2 class="title">
							Itália:<br>
							Certidões atualizadas e pesquisas
						</h2>
					</div>
					<div class="col-md-12 col-lg-8">
						<div class="box">
							<h3 class="subtitle">
								A Itália é um dos países mais procurados pelos brasileiros para o reconhecimento da dupla cidadania.<br>
								O primeiro passo para iniciar esse processo é obter a certidão de nascimento e casamento do italiano que migrou para o Brasil.
							</h3>
						</div>
					</div>
				</div>

				<div class="btn-area">
				<a href="{{url('/busca-de-documentos/italia')}}" title="Saiba mais" class="btn btn-arrow saiba-mais">Saiba mais</a>
				</div>
			</div>
		</div>
		{{-- Slide 3 --}}


		{{-- Slide 4 --}}
		<div class="carousel-item slide4">
			<img src="{{ asset('/images/banner/slide4.jpg') }}" class="d-block w-100" alt="" />

			<div class="container carousel-caption">
				<div class="row size-fix">
					<div class="col-md-12 col-lg-4">
						<h2 class="title">
							Pesquisas Genealógicas
						</h2>
					</div>
					<div class="col-md-12 col-lg-8">
						<div class="box">
							<h3 class="subtitle">
								Conheça seus antepassados, resgate memórias familiares e saiba a partir da pesquisa genealógica se é possível obter sua dupla cidadania estrangeira.
							</h3>
						</div>
					</div>
				</div>

				<div class="btn-area">
				<a href="{{url('/pesquisas-genealogicas')}}" title="Saiba mais" class="btn btn-arrow saiba-mais">Saiba mais</a>
				</div>
			</div>
		</div>
		{{-- Slide 4 --}}

	</div>

	<div class="col-2 col-lg-1 controls">
		<a class="carousel-control-next" href="#main-banner" role="button" data-slide="next">
			<span class="carousel-control-next-icon" aria-hidden="true"></span>
			<span class="sr-only">Next</span>
		</a>

		<a class="carousel-control-prev" href="#main-banner" role="button" data-slide="prev">
			<span class="carousel-control-prev-icon" aria-hidden="true"></span>
			<span class="sr-only">Previous</span>
		</a>
	</div>
</div>


<a href="#servicos"><div class="go-down" data-scroll-to="#servicos"></div></a>

