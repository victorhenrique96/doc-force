<div class="container service-section">
	<div class="row-fluid title-area">
		<h3 class="title-services">
			Nossos<br><strong>Serviços</strong>
		</h3>
	</div>

	<div class="row service-list">
		<div class="col-md-6 service">
			<div class="content">
				<span class="search"></span>
				<h3 class="name">Pesquisa Genealógicas</h3>
				<p class="description">
					Conheça seus antepassados e saiba a partir da pesquisa genealógica se é possível obter sua dupla cidadania.
				</p>
				<div class="btn-area">
					<span class="btn btn-arrow saiba-mais">Saiba mais</span>
				</div>
				<a href="{{ url('/pesquisas-genealogicas') }}" class="link"></a>
			</div>
		</div>

		<div class="col-md-6 service">
			<div class="content">
				<span class="docs"></span>
				<h3 class="name">Busca de Documentos</h3>
				<p class="description">
					A docforce realiza a busca por documentos históricos em outros países e os entrega na sua casa.
				</p>
				<div class="btn-area">
					<span class="btn btn-arrow saiba-mais">Saiba mais</span>
				</div>
				<a href="{{ url('/busca-de-documentos') }}" class="link"></a>
			</div>
		</div>

	</div>
</div>