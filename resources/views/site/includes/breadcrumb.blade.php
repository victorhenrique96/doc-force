<nav aria-label="breadcrumb">
	<ol class="breadcrumb">
		<li class="breadcrumb-item">Você está em</li>
		<li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
		@yield('breadcrumb')
	</ol>
</nav>