@if (isset($paginator) && $paginator->hasPages())

<div class="row">
	<nav class="nav-pagination" aria-label="Paginação">
		<ul class="pagination justify-content-center">

			{{-- Página Anterior --}}
			@if ($paginator->onFirstPage())
				<li class="page-item disabled">
					<a class="page-link prev" href="{{url('#')}}" rel="Página Anterior"><i class="fas fa-arrow-left"></i></a>
					<a class="page-link prev d-none d-sm-flex" href="{{url('#')}}" rel="Página Anterior">Anterior</a>
				</li>
			@else
				<li class="page-item">
					<a class="page-link prev" href="{{ $paginator->previousPageUrl() }}" rel="Página Anterior"><i class="fas fa-arrow-left"></i></a>
					<a class="page-link prev d-none d-sm-flex" href="{{ $paginator->previousPageUrl() }}" rel="Página Anterior">Anterior</a>
				</li>
			@endif

			{{-- Numerador de Páginas --}}
			

			@foreach ($elements as $element)
				{{-- Indicadores da página --}}
				@if (is_string($element))
				<li class="page-item active">
					<a class="page-link" href="#">{{ $element}}</a>
				</li>
				@endif

				{{-- Array de Indicadores --}}
				@if (is_array($element))
					@foreach ($element as $page => $url)
						<li class="page-item"><a class="page-link" href="{{ $url }}">{{ $page }}</a></li>
					@endforeach
				@endif
			@endforeach
			

			{{--  Próxima Página --}}
			@if ($paginator->hasMorePages())
			<li class="page-item">
				<a class="page-link next d-none d-sm-flex" href="{{ $paginator->nextPageUrl() }}" rel="Próxima Página">Próximo</a>
				<a class="page-link next" href="{{ $paginator->nextPageUrl() }}" rel="Próxima Página"><i class="fas fa-arrow-right"></i></a>
			</li>
			@else
			<li class="page-item disabled">
				<a class="page-link next d-none d-sm-flex" href="{{ url('#') }}" rel="Próxima Página">Próximo</a>
				<a class="page-link next" href="{{ url('#') }}" rel="Próxima Página"><i class="fas fa-arrow-right"></i></a>
			</li>
			@endif

		</ul>
	</nav>
</div>

@endif