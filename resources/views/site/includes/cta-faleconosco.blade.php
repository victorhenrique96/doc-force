<div class="row cta fale-conosco">
    <div class="container">
        <div class="contato">
            <h3 class="title">
                Descubra e valorize a história única<br>
                de sua família
            </h3>
            <div class="btn-area">
                <a href="{{ url('/fale-conosco') }}" class="btn btn-default">Fale conosco agora mesmo!</a>
            </div>
        </div>
    </div>
</div>