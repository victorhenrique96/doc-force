<div class="col-12">
	<div class="row parallax">
		<div class="col-md-6 col-lg-5 about-image">
		</div>

		<div class="col-md-6 col-lg-7 content">
			<div class="title-area">
				<h3 class="title">
					<strong>Sobre nós</strong>
				</h3>
				<h4 class="about-description">
					A DocForce é uma empresa especializada no resgate da sua origem.<br>
					Buscamos documentos históricos e pesquisamos genealogia. 
				</h4>
			</div>

			<ul class="about-list">
				<li class="list-item">
					Resgatamos documentos históricos e a memória familiar;
				</li>
				<li class="list-item">
					Buscamos documentos em outros países e entregamos na sua casa;
				</li>
				<li class="list-item">
					Criamos a arvore genealógica da sua família;
				</li>
				<li class="list-item">
					Auxiliamos com suas certidões para o seu processo de dupla cidadania;
				</li>

				<div class="btn-area">
					<a href="{{ url('/fale-conosco') }}" class="btn btn-default fale-conosco">Fale conosco agora mesmo!</a>
				</div>
			</ul>
		</div>
	</div>
</div>
