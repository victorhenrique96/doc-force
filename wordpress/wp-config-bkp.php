<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/pt-br:Editando_wp-config.php
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define( 'DB_NAME', 'docforce' );

/** Usuário do banco de dados MySQL */
define( 'DB_USER', 'victor' );

/** Senha do banco de dados MySQL */
define( 'DB_PASSWORD', '' );

/** Nome do host do MySQL */
define( 'DB_HOST', 'localhost' );

/** Charset do banco de dados a ser usado na criação das tabelas. */
define( 'DB_CHARSET', 'utf8mb4' );

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '9}=KP2~oX$]EW;z64}G}3w_ZS+^cNqpSuypp:Gpb?_<kn}u@JXkV3jS^uB&Z}&uP' );
define( 'SECURE_AUTH_KEY',  ';1N=u[&|RYmAa(T:aBy,isvZls9v<Wl%+/[,IWP98jignKkO|a&`r,N)2nO|+<.P' );
define( 'LOGGED_IN_KEY',    '7ny`vL2&8/>4l$|)^452i5l*eQl#rhqPsWH{ E7G}><_kYv@!*^LJt:]c&Z8Z*<l' );
define( 'NONCE_KEY',        'yd6 }?ir4h,]E(T.9(RNSsrxZ8)p&-=OA6V>!#^ZnKS4zQ5,[Kg]/Z bk6sSM E`' );
define( 'AUTH_SALT',        '$W1;;9~+VEYdf4h85#!mu<Y2%i|fKg;@>} <,jIFB1O(+s|q fawA6Z%VMr@qHUI' );
define( 'SECURE_AUTH_SALT', 'Km>$R(fA SN;%j}-Akc^`O|Um=7:)6[!eqp*-fl<wZFq5[7rmsYWjyI6h^r0<C<;' );
define( 'LOGGED_IN_SALT',   'T1<zWl>9/Yc[g75SF_:tx.rHu[LjR@d]73|_3_6W-y_0Xkc{m]wKTGUw*%bg5Vjm' );
define( 'NONCE_SALT',       'wWj,rj6_?~y`Nmw{QY(t7i7F1ZR`d=D}6shSphTC-Q-A|Y|?HW4U#:R_g1+H_e@z' );

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * um prefixo único para cada um. Somente números, letras e sublinhados!
 */
$table_prefix = 'doc_';

/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://codex.wordpress.org/pt-br:Depura%C3%A7%C3%A3o_no_WordPress
 */
define('WP_DEBUG', false);

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Configura as variáveis e arquivos do WordPress. */
require_once(ABSPATH . 'wp-settings.php');
