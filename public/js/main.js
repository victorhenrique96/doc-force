// Corrige o bug do PARALLAX no IE e EDGE

if ((document.documentMode != undefined) || (/Edge/.test(navigator.userAgent))) {
    document.documentElement.style.overflow = "hidden";
    document.documentElement.style.height = "100%";
    document.body.style.overflow = "auto";
    document.body.style.height = "100%";
}

// Corrigir bug ScrollTop no IE e EDGE

if ((document.documentMode != undefined) || (/Edge/.test(navigator.userAgent))) {

	var PageY = pageYOffset;

	const whatsapp = document.querySelector('#faleConosco');

	$(window).bind('mousewheel', function(event) {
		if (event.originalEvent.wheelDelta >= 0) {
			PageY -= 1;
			if(PageY < 0){
				whatsapp.classList.remove('ie-btn-fix');
				PageY = 0;
			}
		}
		else {
			PageY += 1;
			if(PageY > 0){
				whatsapp.classList.add('ie-btn-fix');
			}
		}
		});

	whatsapp.addEventListener('mouseover', function(event){
		whatsapp.classList.remove('ie-btn-fix');
	}, true);
}


// Corrigir bug ScrollDown Button

if ((document.documentMode != undefined) || (/Edge/.test(navigator.userAgent))) {

	let anchorlinks = document.querySelectorAll('a[href^="#"]')
 
	for (let item of anchorlinks) { // relitere 
		item.addEventListener('click', (e)=> {
			let hashval = item.getAttribute('href')
			let target = document.querySelector(hashval)
			target.scrollIntoView({
				behavior: 'smooth',
				block: 'start'
			})
			history.pushState(null, null, hashval)
			e.preventDefault()
		})
	}

}




var start = {
	breakpoints: {
		mobile: '(max-width: 767px)',
		tablet: '(min-width: 768px) and (max-width: 991px)',
		desktop: '(min-width: 992px)'
	},

	functions: {
		orcamentoFixed: function(altura){
			if ((window.matchMedia(start.breakpoints.mobile).matches == false) && (window.matchMedia(start.breakpoints.tablet).matches == false)){
				if ($(window).scrollTop() > 0){
					$('#faleConosco').addClass('fixed');
				}else{
					$('#faleConosco').removeClass('fixed');
				}
			}
		},

		smoothAncora: function (){
			$(".anchor").on('click', function(event){
				if (this.hash !== "") {
					// event.preventDefault();
					var hash = this.hash;

					$('html, body').animate({
						scrollTop: ($(hash).offset().top - 80)
					}, 800, function(){
						window.location.hash = hash;
					});
				}
			});

			$(".clean-anchor").on('click', function(event){
				if (this.hash !== "") {
					// event.preventDefault();
					var hash = this.hash;

					$('html, body').animate({
						scrollTop: ($(hash).offset().top - 80)
					}, 800, function(){
						window.location.hash = hash;
						window.history.replaceState({}, document.title, (window.location.href).replace(hash, ''));
					});
				}
			});
		}
	},

	events: {
		init: function(){
			$(document).ready(function(){
				$('.carousel').carousel();

				var altura = $('#faleConosco').offset().top;
				start.functions.orcamentoFixed(altura);

				$(window).scroll(function (){
					start.functions.orcamentoFixed(altura);
				});
			});
		},

		scroller: function(){
			$('[data-scroll-to]').on('click', function(e){    
				e.preventDefault();      
				$('html, body').animate({ scrollTop: $($(this).data('scroll-to')).offset().top }, 'slow');
			});           
		}
	},

	init: function(){
		this.events.init();
		//this.events.ancora();
		this.events.scroller();
		// this.plugins.mask();
	}
}

start.init();


// Mask para Telefone

const InputTelefone = document.querySelector('#telefone_contato');

var behavior = function (val) {
    return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
},
options = {
    onKeyPress: function (val, e, field, options) {
        field.mask(behavior.apply({}, arguments), options);
    }
};

$(document).ready(function (){
	$(InputTelefone).mask(behavior, options);
});
