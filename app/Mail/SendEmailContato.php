<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendEmailContato extends Mailable
{
    use Queueable, SerializesModels;
    public $data;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Contato Doc Force')
                    ->view('email.contato')
                    ->with([
                        'nome' => $this->data["nome"],
                        'email' => $this->data["email"],
                        'tel' => $this->data["telefone"],
                        'mensagem' => $this->data["mensagem"],
                    ]);
    }
}
