<?php
/*
 * Retorna a url das áreas do template atual.
 *
 */
if (!function_exists('getUrl')){

	function getUrl($url = '/')
	{
		$make_url = '';

		if(env('MODEL_TYPE') && env('MODEL_NUMBER')){
			if(env('MODEL_TYPE') == 'singlepage' || env('MODEL_TYPE') == 'institucional'){
				$make_url = (env('MODEL_TYPE') == 'singlepage')? '/singlepages/' : '/institucionais/';
				$make_url .= 'modelo-' . env('MODEL_NUMBER') . '/';
			}

		}elseif(Request::segment(1) && Request::segment(2)){
			
			if(Request::segment(1) == 'singlepages' || Request::segment(1) == 'institucionais'){
				$make_url = (Request::segment(1) == 'singlepages')? '/singlepages/' : '/institucionais/';
				$make_url .= Request::segment(2) . '/';
			}

		}else{
			$make_url = '/';
		}

		if($url != '/'){
			$make_url .= (substr($url, 0, 1) == '/')? substr_replace($url, '', 0, 1) : $url;
		}

		return url($make_url);
	}
}

/*
 * Retorna a url da pasta de assets (na pasta public = css, js, images) do template atual.
 *
 */
if (!function_exists('getAssetUrl')){

	function getAssetUrl($url = '/')
	{
		$make_url = '';

		if(env('MODEL_TYPE') && env('MODEL_NUMBER')){
			if(env('MODEL_TYPE') == 'singlepage' || env('MODEL_TYPE') == 'institucional'){
				$make_url = (env('MODEL_TYPE') == 'singlepage')? '/singlepage' : '/institucional';
				$make_url .= '_' . env('MODEL_NUMBER') . '/';
			}

		}elseif(Request::segment(1) && Request::segment(2)){
			
			if(Request::segment(1) == 'singlepages' || Request::segment(1) == 'institucionais'){
				$make_url = (Request::segment(1) == 'singlepages')? '/singlepage' : '/institucional';

				if(strpos(Request::segment(2), 'modelo-') !== FALSE){
					$make_url .= '_' . (substr(Request::segment(2), strpos(Request::segment(2), "-", 1) + 1));
					$make_url .= '/';
				}
			}

		}else{
			$make_url = '/';
		}

		if($url != '/'){
			$make_url .= (substr($url, 0, 1) == '/')? substr_replace($url, '', 0, 1) : $url;
		}

		return asset($make_url);
	}
}

/*
 * Pega o nome do template atual => "singlepage_1", "institucional_2"
 *
 */
if (!function_exists('getTemplateName')){

	function getTemplateName($include = FALSE)
	{
		$make_template_name = '';

		if(env('MODEL_TYPE') && env('MODEL_NUMBER')){
			if(env('MODEL_TYPE') == 'singlepage' || env('MODEL_TYPE') == 'institucional'){
				$make_template_name = (env('MODEL_TYPE') == 'singlepage')? 'singlepage' : 'institucional';
				$make_template_name .= '_' . env('MODEL_NUMBER');
			}

		}elseif(Request::segment(1) && Request::segment(2)){
			
			if(Request::segment(1) == 'singlepages' || Request::segment(1) == 'institucionais'){
				$make_template_name = (Request::segment(1) == 'singlepages')? 'singlepage' : 'institucional';

				if(strpos(Request::segment(2), 'modelo-') !== FALSE){
					$make_template_name .= '_' . (substr(Request::segment(2), strpos(Request::segment(2), "-", 1) + 1));
				}
			}
		}

		if($include && !empty($make_template_name)){
			$make_template_name .= '.';
		}

		return $make_template_name;
	}
}

/*
 * Verifica se a página atual é a Homepage
 *
 */
if (!function_exists('isHome')){

	function isHome()
	{
		$result = FALSE;

		if(\Route::has('home') && (\Route::current()->getName() == 'home')){
			$result = TRUE;
		}else{
			if(!Request::segment(1) || Request::segment(1) == 'home'){
				$result = TRUE;

			}elseif(Request::segment(1) && Request::segment(2)){
				if(Request::segment(1) == 'singlepages' || Request::segment(1) == 'institucionais'){
					if(strpos(Request::segment(2), 'modelo-') !== FALSE){
						if(!Request::segment(3) || Request::segment(3) == 'home'){
							$result = TRUE;
						}
					}
				}
			}
		}

		return $result;
	}
}