<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Corcel\Model\Post as Corcel;

class Post extends Corcel
{
    protected $connection = 'wordpress';

    public function index(){}

    public function getPostDateAttribute($value)
    {
        return date('d/m/Y', strtotime($value));
    }

    public function getPostContentAttribute($value)
    {
        $stringWithBRs = nl2br($value);
        $stringWithPs = str_replace("<br /><br />", "</p>\n<p>", $stringWithBRs);
        $stringWithPs = "<p>" . $stringWithPs . "</p>";
        return $stringWithPs;
    }

    public function getMetasAttribute()
    {
        $metas = [];
        foreach ($this->meta as $meta) {
            $metas[$meta->meta_key] = $meta->value;
        }
        return $metas;
    }

    public function getSeoAttribute()
    {
        $metas = $this->metas;

        return [
            'keywords' => !empty($metas['_yoast_wpseo_focuskw']) ? $metas['_yoast_wpseo_focuskw'] : '',
            'title' => !empty($metas['_yoast_wpseo_title']) ? $metas['_yoast_wpseo_title'] : '',
            'description' => !empty($metas['_yoast_wpseo_metadesc']) ? $metas['_yoast_wpseo_metadesc'] : '',
            'metakeywords' => !empty($metas['_yoast_wpseo_metakeywords']) ? $metas['_yoast_wpseo_metakeywords'] : '',
            'noindex' => !empty($metas['_yoast_wpseo_meta-robots-noindex']) ? $metas['_yoast_wpseo_meta-robots-noindex'] : '',
            'nofollow' => !empty($metas['_yoast_wpseo_meta-robots-nofollow']) ? $metas['_yoast_wpseo_meta-robots-nofollow'] : '',
            'opengraph-title' => !empty($metas['_yoast_wpseo_opengraph-title']) ? $metas['_yoast_wpseo_opengraph-title'] : '',
            'opengraph-description' => !empty($metas['_yoast_wpseo_opengraph-description']) ? $metas['_yoast_wpseo_opengraph-description'] : '',
            'image' => !empty($metas['_yoast_wpseo_opengraph-image']) ? $metas['_yoast_wpseo_opengraph-image'] : '',
            'twitter-title' => !empty($metas['_yoast_wpseo_twitter-title']) ? $metas['_yoast_wpseo_twitter-title'] : '',
            'twitter-description' => !empty($metas['_yoast_wpseo_twitter-description']) ? $metas['_yoast_wpseo_twitter-description'] : '',
            'twitter-image' => !empty($metas['_yoast_wpseo_twitter-image']) ? $metas['_yoast_wpseo_twitter-image'] : '',
        ];
    }
}