<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SendContatoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return  [
            'nome_contato' => 'required|max:600',
            'email_contato' => 'required|email|max:600',
            'telefone_contato' => 'required|max:600',
            'mensagem_contato' => 'required|max:600'
        ];
    }

    public function messages()
    {
        return [
            'required' => 'Todos os campos são obrigatórios.',
            'email.email' => 'Forneça um endereço de e-mail válido.',
            'nome.max' => 'Forneça um nome válido.',
            'email.max' => 'Forneça um endereço de e-mail válido.',
            'telefone.max' => 'Forneça um número de telefone válido.',
            'msg.max' => 'Tamanho da mensagem muito grande.'
        ];
    }
}
