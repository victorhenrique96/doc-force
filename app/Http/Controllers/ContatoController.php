<?php

namespace App\Http\Controllers;

use App\Mail\SendEmailContato;
use App\Http\Requests\SendContatoRequest;
use Illuminate\Http\Request;
use Mail;
Use Exception;

class ContatoController extends Controller
{
    public function sendEmail(SendContatoRequest $req)
    {
        $data = [
            'nome' => $req->nome_contato,
            'email' => $req->email_contato,
            'telefone' => $req->telefone_contato,
            'mensagem' => nl2br($req->mensagem_contato)
        ];

        try {
            Mail::to(env('MAIL_ADDRESS_TO', $data['email']))->send(new SendEmailContato($data));
            return redirect()->back()->with('success', 'Dados enviados com sucesso!');
        } catch (Exception $e) {
            return redirect()->back()->with('error', 'Não foi possível enviar a mensagem!');
        }
        return;

    }
}
