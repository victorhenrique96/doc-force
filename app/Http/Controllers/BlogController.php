<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Models\Post;
use App\Http\Controllers\Controller;

class BlogController extends Controller
{
    public function index(Request $request)
    {
        $search = $request->get('search');

        if($search){
            $posts = Post::published()
                ->search($search)
                ->whereIn('post_type', ['post'])
                ->newest()->paginate(4);
        }else{
            $posts = Post::published()
                ->whereIn('post_type', ['post'])
                ->newest()->paginate(4);
        }
        
        return view('site.blog.blog', ['posts' => $posts]);
    }



    public function show($name)
    {
        $post = Post::slug($name)->with('attachment')->first();
        if(!$post){
            return view('site.blog.404');
        }
        $post->meta_description = strip_tags($post->content);
        $post->meta_description = Str::limit(trim($post->meta_description), 120);
        
        
        $other_posts = Post::published()
                        ->newest()->limit(2)
                        ->whereIn('post_type', ['post'])
                        ->where('id', '<>', $post->ID)->get();
        return view('site.blog.integra', ['post' => $post, 'other_posts' => $other_posts]);
    }
}
