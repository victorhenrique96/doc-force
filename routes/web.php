<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('site.index');
});

Route::get('/sobre-nos', function () {
    return view('site.sobre-nos');
});

Route::get('/pesquisas-genealogicas', function () {
    return view('site.pesquisa-genealogica');
});

Route::get('/busca-de-documentos', function () {
    return view('site.buscadocumentos');
});

// Busca de documentos Integra
    Route::get('/busca-de-documentos/brasil', function () {
        return view('site.buscadocumentos-integra');
    });

    Route::get('/busca-de-documentos/espanha', function () {
        return view('site.buscadocumentos-integra');
    });

    Route::get('/busca-de-documentos/italia', function () {
        return view('site.buscadocumentos-integra');
    });

    Route::get('/busca-de-documentos/alemanha-austria-polonia', function () {
        return view('site.buscadocumentos-integra');
    });

    Route::get('/busca-de-documentos/portugal', function () {
        return view('site.buscadocumentos-integra');
    });

    Route::get('/busca-de-documentos/demais-certidoes', function () {
        return view('site.buscadocumentos-integra');
    });


Route::get('/fale-conosco', function () {
    return view('site.contato');
});

Route::post('fale-conosco', 'ContatoController@sendEmail')->name('contato');

// Route::get('/blog', function () {
//     return view('site.blog-listagem');
// });

// Route::get('/blog/integra', function () {
//     return view('site.blog-integra');
// });

// Blog ==============================================================================
Route::group(['prefix' => 'blog'], function () {
    Route::get('/', 'BlogController@index')->name('site.blog.blog');

    Route::get('/{name}', 'BlogController@show')->name('site.blog.integra');
});

// Paginas de Erros
